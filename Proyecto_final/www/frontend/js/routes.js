angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



      .state('mENU.partidas', {
    url: '/games',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/partidas.html',
        controller: 'gamesCtrl'
      }
    }
  })

  .state('mENU.instalaciones', {
    url: '/facility',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/instalaciones.html',
        controller: 'facilityCtrl'
      }
    }
  })

  .state('mENU.miPerfil', {
    url: '/myprofile',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/miPerfil.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('mENU', {
    url: '/menu',
    templateUrl: 'frontend/templates/mENU.html',
    abstract:true
  })

  .state('login', {
    url: '/',
    templateUrl: 'frontend/templates/login.html',
    controller: 'loginCtrl'
  })

  .state('mENU.nuevaPartida', {
    url: '/newgame',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/nuevaPartida.html',
        controller: 'newgameCtrl'
      }
    }
  })

  .state('mENU.modificarDatos', {
    url: '/modify',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/modificarDatos.html',
        controller: 'modifyDataCtrl'
      }
    }
  })

  .state('mENU.crearInstalaciN', {
    url: '/newfacility',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/crearInstalaciN.html',
        controller: 'newFacilityCtrl'
      }
    }
  })

  .state('mapaPartidas', {
    url: '/map',
    templateUrl: 'frontend/templates/mapaPartidas.html',
    controller: 'mapsGamesCtrl'
  })

  .state('signup', {
    url: '/singup',
    templateUrl: 'frontend/templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('recordarPassword', {
    url: '/recovery',
    templateUrl: 'frontend/templates/recordarPassword.html',
    controller: 'recoveryPasswordCtrl'
  })

  .state('partida', {
    url: '/descripgame',
    templateUrl: 'frontend/templates/partida.html',
    controller: 'gameCtrl'
  })

  .state('mENU.settings', {
    url: '/settings',
    views: {
      'side-menu21': {
        templateUrl: 'frontend/templates/settings.html',
        controller: 'settingsCtrl'
      }
    }
  })

  .state('usuarios', {
    url: '/usersplay',
    templateUrl: 'frontend/templates/usuarios.html',
    controller: 'usersCtrl'
  })

  .state('ubicacion', {
    url: '/mapgame',
    templateUrl: 'frontend/templates/ubicacion.html',
    controller: 'Ctrl'
  })

  .state('chat', {
    url: '/chat',
    templateUrl: 'frontend/templates/chat.html',
    controller: 'chatCtrl'
  })

$urlRouterProvider.otherwise('/')



});
