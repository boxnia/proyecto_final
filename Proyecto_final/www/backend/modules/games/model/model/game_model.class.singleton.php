<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/NFU/';
define('SITE_ROOT', $path);
require(SITE_ROOT . "modules/games/model/BLL/game_bll.class.singleton.php");

class game_model {

    private $bll;
    static $_instance;

    private function __construct() {        
        /*$this->bll = gamebll::getInstance();*/
        $this->bll = game_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_game($arrArgument) {             
        return $this->bll->create_game_BLL($arrArgument);        
    }

    public function create_ins($arrArgument) {             
        return $this->bll->create_ins_BLL($arrArgument);        
    }
    /*public function obtain_countries($url) {
        return $this->bll->obtain_countries_BLL($url);
    }*/

    public function obtain_provinces() {
        return $this->bll->obtain_provinces_BLL();
    }

    public function obtain_populations($arrArgument) {
        return $this->bll->obtain_populations_BLL($arrArgument);
    }

    public function filter_install_nombre($criteria) {
       
        return $this->bll->filter_install_nombre_BLL($criteria);
    }
    
    public function get_coor_install($criteria) {
       
        return $this->bll->get_coor_install_BLL($criteria);
    }
}
