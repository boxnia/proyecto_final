<?php

class controller_main {

    function __construct() {
          $_SESSION['module'] = "main";
       // include(UTILS . "common.inc.php");
    }

    function begin() {
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/main/view/', 'main.php');

        require_once(VIEW_PATH_INC . "footer.html");
      
    }
    
     ///////////////////////////////////////loal_all_games

    public function load_coordinates_near() {
        if (isset($_POST["coord"])) {

            $page = intval($_POST["coord"]);
            $usersJSON = json_decode($_POST["coord"], true);
            $page = intval($usersJSON['p']);
            set_error_handler('ErrorHandler');



            $current_page = $page - 1;
            $records_per_page = 3; // records to show per page
            $start = $current_page * $records_per_page;

            $arrargument = array(
                'start' => $start,
                'records_per_page' => $records_per_page,
                'latitud' => $usersJSON['lat_position'],
                'logitud' => $usersJSON['long_position'],
            );

            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_USERS, "users_model", "obtain_coord_near", $arrargument);


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                $results = false;
            }
            restore_error_handler();

            if ($results) {


                $jsondata["game"] = $results;

                $jsondata["success"] = true;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                $jsondata["success"] = false;
                $jsondata["msje"] = "Error en la consulta partidas";
                exit;
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["msje"] = "Error en la consulta partidas";
            exit;
        }
    }

}
