<?php

//require(BLL_USERS . "userBLL.class.singleton.php");
    
class users_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = users_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function update_user($arrArgument) {
                         

        return $this->bll->update_user_BLL($arrArgument);
    }

      public function activateUser($arrArgument) {
        
        return $this->bll->activateUser_BLL($arrArgument);
    }

    public function obtain_paises($url) {
        return $this->bll->obtain_paises_BLL($url);
    }

    public function obtain_provincias() {
        return $this->bll->obtain_provincias_BLL();
    }

    public function obtain_poblaciones($arrArgument) {
        return $this->bll->obtain_poblaciones_BLL($arrArgument);
    }
    
    public function obtain_users() {
        return $this->bll->obtain_users_BLL();
    }
    public function obtain_coord() {
        return $this->bll->obtain_coord_BLL();
    }
      public function obtain_coord_near($arrArgument) {
         
        return $this->bll->obtain_coord_near_BLL($arrArgument);
    }

      public function obtain_user($arrArgument) {
         
        return $this->bll->obtain_user_BLL($arrArgument);
    }
    
      public function obtain_coord_near_map($arrArgument) {
         
        return $this->bll->obtain_coord_near_map_BLL($arrArgument);
    }
  public function count_games() {
         
        return $this->bll->count_games_BLL();
    }
    
      public function exist_users_email($arrArgument) {
       return $this->bll->exist_users_email_BLL($arrArgument);
    }
 
      public function obtain_password($arrArgument) {
           
       return $this->bll->obtain_password_BLL($arrArgument);
    }
      public function delete_user($arrArgument) {
           
       return $this->bll->delete_user_BLL($arrArgument);
    }
    
       public function exist_users_email_only($arrArgument) {
       return $this->bll->exist_users_email_only_BLL($arrArgument);
    }
    
     public function details_users($arrArgument) {
       
       return $this->bll->details_users_BLL($arrArgument);
    }
}
