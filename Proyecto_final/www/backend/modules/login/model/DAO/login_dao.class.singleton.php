<?php

class login_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user_DAO($db, $arrArgument) {
        $codigo = '';
        $nombre = $arrArgument['nombre'];
        $apellidos = '';
        $direccion = '';
        $numero = '';
        $poblacion = '';
        $provincia = '';
        $pais = '';
        $deportes = '';

        $deportes1 = 0;
        $deportes2 = 0;
        $deportes3 = 0;
        $deportes4 = 0;
        $deportes5 = 0;
        $deportes6 = 0;

        /* foreach ($deportes as $indice) {

          if ($indice == "Todos") {

          $deportes1 = 1;
          }
          if ($indice === "futbol") {
          $deportes2 = 1;
          }
          if ($indice === "baloncesto") {
          $deportes3 = 1;
          }
          if ($indice === "voleibol") {
          $deportes4 = 1;
          }
          if ($indice === "tenis") {
          $deportes5 = 1;
          }
          if ($indice === "padel") {
          $deportes6 = 1;
          }
         */
//echo "El valor de $clave es: $valor";
        // }
        $nivel = '';
        $email = $arrArgument['email'];
        $password = $arrArgument['password'];
        $fecha_nac = $arrArgument['fecha_nac'];
        $fecha_registro = $arrArgument['fecha_registro'];
        $avatar = $arrArgument['avatar'];
        $token = $arrArgument['token'];
        $status = $arrArgument['status'];
        $tipo = $arrArgument['tipo'];
        $codigo = $arrArgument['codigo'];
        
        $sql = "INSERT INTO users ( nombre, apellidos, direccion, numero, poblacion, provincia, todos, futbol, baloncesto, voleibol, tenis, padel, nivel, email,"
                . " password, fecha_nac, fecha_registro, avatar, pais,codigo,token,status,tipo"
                . " ) VALUES ( '$nombre', '$apellidos', '$direccion', '$numero', '$poblacion', '$provincia', '$deportes1', '$deportes2', '$deportes3', '$deportes4', '$deportes5', '$deportes6','$nivel',"
                . " '$email', '$password', '$fecha_nac', '$fecha_registro', '$avatar', '$pais','$codigo','$token','$status','$tipo')";

        
     
        return $db->ejecutar($sql);
    }

    public function obtain_paises_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }

    public function obtain_provincias_DAO() {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_poblaciones_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        $xml = simplexml_load_file(RESOURCES . "provinciasypoblaciones.xml");
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function details_users_DAO($db, $arrArgument) {


        $sql = "SELECT * FROM users WHERE nombre='$arrArgument'";

        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function exist_users_nombre_DAO($db, $arrArgument) {



        $sql = "SELECT COUNT(*) AS total_nombre FROM users WHERE nombre = '$arrArgument'   ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function exist_users_email_DAO($db, $arrArgument) {



        $sql = "SELECT COUNT(*) AS total_email FROM users WHERE email = '$arrArgument'  ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function validate_email_recover_DAO($db, $arrArgument) {

        /* $sql = "UPDATE users
          SET token=value1
          WHERE email='$arrArgument'"; */

        $sql = "SELECT token FROM users WHERE email = '$arrArgument'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function update_email_recover_DAO($db, $arrArgument) {
        $email = $arrArgument['email'];
        $token = $arrArgument['token'];


        $sql = "UPDATE users SET token='$token' WHERE email='$email'";

        return $db->ejecutar($sql);
    }

    public function update_pass_recover_DAO($db, $arrArgument) {
        $pass = $arrArgument['pass'];
        $token = $arrArgument['token'];


        $sql = "UPDATE users SET password='$pass' WHERE token='$token'";

        return $db->ejecutar($sql);
    }

    public function users_exist_DAO($db, $arrArgument) {



        $sql = "SELECT * FROM users WHERE nombre = '$arrArgument'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function users_exist_codigo_DAO($db, $arrArgument) {



        $sql = "SELECT COUNT(*) AS total_codigo FROM users WHERE codigo = '$arrArgument'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}
