<?php

class installation_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_install_DAO($db, $arrArgument) {
        /* $codigo = $arrArgument['codigo'];
          $nombre = $arrArgument['nombre'];
          $apellidos = $arrArgument['apellidos'];
          $direccion = $arrArgument['direccion'];
          $numero = $arrArgument['numero'];
          $poblacion = $arrArgument['poblacion'];
          $provincia = $arrArgument['provincia'];

          $deportes = $arrArgument['deportes'];

          $deportes1 = 0;
          $deportes2 = 0;
          $deportes3 = 0;
          $deportes4 = 0;
          $deportes5 = 0;
          $deportes6 = 0;

          foreach ($deportes as $indice) {

          if ($indice == "Todos") {

          $deportes1 = 1;
          }
          if ($indice === "futbol") {
          $deportes2 = 1;
          }
          if ($indice === "baloncesto") {
          $deportes3 = 1;
          }
          if ($indice === "voleibol") {
          $deportes4 = 1;
          }
          if ($indice === "tenis") {
          $deportes5 = 1;
          }
          if ($indice === "padel") {
          $deportes6 = 1;
          }

          //echo "El valor de $clave es: $valor";
          }
          $nivel = $arrArgument['nivel'];
          $email = $arrArgument['email'];
          $password = $arrArgument['password'];
          $fecha_nac = $arrArgument['fecha_nac'];
          $fecha_registro = $arrArgument['fecha_registro'];
          $avatar = $arrArgument['avatar'];

          $sql = "INSERT INTO users (codigo, nombre, apellidos, direccion, numero, poblacion, provincia, todos, futbol, baloncesto, voleibol, tenis, padel, nivel, email,"
          . " password, fecha_nac, fecha_registro, avatar"
          . " ) VALUES ('$codigo', '$nombre', '$apellidos', '$direccion', '$numero', '$poblacion', '$provincia', '$deportes1', '$deportes2', '$deportes3', '$deportes4', '$deportes5', '$deportes6','$nivel',"
          . " '$email', '$password', '$fecha_nac', '$fecha_registro', '$avatar')";

          return $db->ejecutar($sql);
          }

          public function list_install_DAO($db, $arrArgument) {


          return $db->ejecutar($sql); */
    }

    public function list_install_DAO($db) {
        
        $sql = "SELECT * FROM installation";

        $stmt = $db->ejecutar($sql);

        return $db->listar($stmt);
    }

    public function details_install_DAO($db, $id) {

        $sql = "SELECT * FROM installation WHERE id='$id'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function order_install_DAO($db, $arrArgument) {

        $position = $arrArgument['position'];
        $item = $arrArgument['item'];

        $sql = "SELECT * FROM installation ORDER BY id ASC LIMIT $position,$item";
        $stmt = $db->ejecutar($sql);


        return $db->listar($stmt);
    }

    public function count_install_DAO($db, $id) {

        $sql = "SELECT COUNT(*) AS total FROM installation";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function order_install_nombre_DAO($db) {


        $sql = "SELECT * FROM installation ORDER BY nombre";
        $stmt = $db->ejecutar($sql);


        return $db->listar($stmt);
    }

    public function filter_install_nombrel_DAO($db, $criteria) {


        $sql = "SELECT DISTINCT * FROM installation WHERE nombre like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);


        return $db->listar($stmt);
    }

    public function count_install_criteria_DAO($db, $criteria) {

        $sql = "SELECT COUNT(*) AS total_filter FROM installation WHERE nombre like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function order_install_criteria_DAO($db, $arrArgument) {

        $position = $arrArgument['position'];
        $item = $arrArgument['item'];
        $criteria = $arrArgument['criteria'];

        $sql = "SELECT * FROM installation  WHERE nombre like '%" . $criteria . "%'ORDER BY id ASC LIMIT $position,$item";
        $stmt = $db->ejecutar($sql);


        return $db->listar($stmt);
    }

}
